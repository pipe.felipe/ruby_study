i = 0
# while i < 5
#   puts i
#   i = i + 1
# end

puts "Usando o while em uma linha: o i é: #{i += 1}" while i < 5

puts "\n"

until i < 5
  puts "until #{i}"
  break if i > 5
  i += 1
end

puts "\n"

loop do
  puts "loop do"
  i += 1
  break if i > 5
end

4.times do |i|
  puts "utilizando o times #{i}"
end

puts "\n"

2.times { |i| puts "uma linha times #{i}" }

puts "\n"

(1...5).each { |i| puts "(1...5)each #{i}" }

puts "\n"

[1, 2, 3].each { |i| puts "[1, 2, 3].each #{i}" }

puts "\n"

for i in 0..7
  next if i % 2 == 0
  puts i
end
