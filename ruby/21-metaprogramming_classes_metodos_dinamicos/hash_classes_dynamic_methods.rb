require "byebug"

def definir_classe(classe, nome_metodo)
  if (classe.is_a?(String))
    classe = classe.capitalize
    eval("class #{classe} end")
    classe = Object.const_get(classe)
  end

  classe.class_eval do
    define_method(nome_metodo) do |*params|
      puts "O valor dos parâmetros é: #{params.join(", ")}"
    end
  end
end

{
  "Felipe" => ["vencer", "agradecer", "ajudar"],
  "Enaig" => ["encontrar", "amar", "ficar_feliz"],
}.each do |classe, metodos|
  metodos.each do |metodo|
    definir_classe(classe, metodo)
  end
end

puts Felipe.new.methods - Class.methods
puts "\n"
puts Enaig.new.methods - Class.methods
