require "byebug"

def definir_classe(classe)
  classe = classe.capitalize

  eval("class #{classe} end")
  classe = Object.const_get(classe)
end

["classe1", "classe2", "classe3"].each do |classe|
  definir_classe(classe)
end

debugger

x = 0
