require "byebug"

def definir_classe(classe, nome_metodo)
  if (classe.is_a?(String))
    classe = classe.capitalize
    eval("class #{classe} end")
    classe = Object.const_get(classe)
  end

  classe.class_eval do
    define_method(nome_metodo) do |*params|
      puts "O valor dos parâmetros é: #{params.join(", ")}"
    end
  end
end

["mostrar", "exibir", "visualizar"].each do |metodo|
  definir_classe("felipe", metodo)
end

debugger

["mostrar", "exibir", "visualizar"].each do |metodo|
  Felipe.new.send(metodo, 1, 2, 3)
end

# Chamando todos os dinâmicos

["Felipe", "Xoutrs", "Enaig"].each do |classe|
  ["mostrar", "exibir", "visualizar"].each do |metodo|
    definir_classe(classe, metodo)
    Object.const_get(classe).new.send(metodo, 1, 2, 3)
  end
end

debugger

x = 0
