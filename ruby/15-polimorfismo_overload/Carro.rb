class Carro
  def initialize(nome = "Modelo")
    @nome = nome
  end

  attr_accessor :nome, :porta, :painel, :roda
  attr_reader :pneu

  def show_car(marca = "Padrão")
    puts "Marca: #{marca} - Modelo: #{self.nome}"
  end

  def initialize(nome = "Modelo")
    @nome = nome
  end

  attr_accessor :nome, :porta, :painel, :roda
  attr_reader :pneu

  def mostrar(marca = "Padrão")
    puts "Marca: #{marca} - Modelo: #{self.nome}"
    puts algo_mais
  end

  private

  def algo_mais
    puts "Método privado em Carros.rb para retornar algo a mais."
  end

  def algo_mais2
    puts "Método privado em Carros.rb para retornar algo a mais."
  end

  def algo_mais3
    puts "Método privado em Carros.rb para retornar algo a mais."
  end

  def algo_mais4
    puts "Método privado em Carros.rb para retornar algo a mais."
  end

  public

  def algo_mais_publico
    puts "PUBLICO: Método publico em Carros.rb para retornar algo a mais."
  end

  protected

  def algo_mais_protected
    puts "PROTECTED: Método protected em Carros.rb para retornar algo a mais."
  end
end
