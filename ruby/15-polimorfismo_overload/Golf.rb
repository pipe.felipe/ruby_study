class Golf < Carro
  def mostrar
    mostrar_original = super
    puts "Este é o método do Golf #{mostrar_original}"
  end

  def andar(a, b, c)
    puts "andar com parametros"
  end

  # def andar
  #   puts "andar sem parametros"
  # end

  def andar(*args)
    puts "andar com parametros opicionais #{args}"

    args.each do |param|
      puts param
    end
  end
end
