# PÚBLICO.
# Ações  e atributos que são externas.

# PRIVADO.
# Serve apenas internamente.
# Por exemplo. Carro.nome (e esse carro é privado) não é possível acessar.
# O provado funciona apenas dentro do class e o 'end' definido.
#
# class Felipe
#   Tudo aqui dentro, de escopo privado, funcionará apenas até o end.
# end

# PROTECTED.
# Fica interno dentro da classe. Podendo acessar apenas dentro da instância.
# A diferença entre o protected e o private é: Quando há uma herança, o atribu-
# to protected ainda é visível para a dentro da classe que herdou da class mãe.

require "byebug"
require_relative "Carro"
require_relative "Golf"
require_relative "Fiesta"

golf = Golf.new
fiesta = Fiesta.new
carro = Carro.new

debugger

Carro.new.mostrar
