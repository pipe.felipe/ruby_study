require "byebug"

# Proc são semelhante ao lambda,
# mas aceitam mais de um parâmetro ou
# até mesmo não necessita de parâmetro nenhum.

l = Proc.new do |param|
  param = 0 if param.nil?
  param * 5
end
puts l.call(4)

debugger

l2 = Proc.new do |p1, p2|
  p1 + p2
end
puts l.call(4)

debugger

x = ""
