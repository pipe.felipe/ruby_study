require "byebug"

module AtributosDinamicos
  def atributos(*args)
    args.each do |arg|
      define_method("#{arg}=") do |value|
        instance_variable_set "@#{arg}", value
      end

      define_method("#{arg}") do
        instance_variable_get "@#{arg}"
      end
    end
  end
end

class Teste
  extend AtributosDinamicos
  atributos :nome, :telefone
end

t = Teste.new
t.nome = "felipe"
t.telefone = 0

puts "#{t.nome}, #{t.telefone}"
