require "byebug"

# class Test
#   def inicio
#     "inicio"

#     def fim
#       "fim"
#     end
#   end
# end

# t = Test.new
# debugger
# t.inicio
# t.fim

# Define Method é semelhante ao eval
# mas de maneira mais 'limpa'.

class Teste
  def self.definir(nome_do_metodo)
    define_method(nome_do_metodo) do |param1, param2|
      puts "#{param1} #{param2}"
    end
  end

  def self.atributo(nome_do_metodo)
    define_method(nome_do_metodo) do |param|
      puts "#{param}"
    end
  end
end

Teste.definir("novo_metodo")
["set_nome", "set_telefone", "set_endereço"].each do |nomes_metodos|
  Teste.atributo(nomes_metodos)
end
teste = Teste.new
debugger
# Teste.new.novo_metodo("Felipe", "que legal")
x = ""
