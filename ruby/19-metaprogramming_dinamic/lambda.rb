require "byebug"

# lambda não aceita mais parametros do que os definidos.
# lambda retorno dentro do próprio lambda.

l = lambda { |parametros| parametros * 5 }
puts l.call(4)

l2 = lambda do |p1, p2|
  p1 + p2
end

puts l2.call(4, 5)

debugger

x = " "
