require "byebug"

# def metodo_para_definir_bloco(*bloco)
#   debugger
#   bloco
# end

# metodo_para_definir_bloco(1, 2, 3, 4, 5)

# Esse método em bloco, é semelhante aos call backs em JS.
def metodo_para_definir_bloco(&bloco)
  puts "dentro do metódo"
  bloco
end

l = metodo_para_definir_bloco do |param|
  param * 5
end

puts l.call (4)

l = metodo_para_definir_bloco do
  puts "felipe"
end

l.call

x = ""
