class Carro
  def initialize(nome = "Modelo")
    @nome = nome
  end

  attr_accessor :nome, :porta, :painel, :roda
  attr_reader :pneu

  def show_car(marca = "Padrão")
    puts "Marca: #{marca} - Modelo: #{self.nome}"
  end
end