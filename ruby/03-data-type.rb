require 'byebug'

# a = 1
# h = {}

# debugger  

# puts a.class
# puts nil.class
# puts :symbol.class
# puts h.class
# puts [].class
# puts (1..8).class

# a = 1
# puts a.class

# @a = 2
# puts @a.class

# @@a = 3
# puts @@a

# $a = 4
# puts $a

# A = 5
# puts A
#

class Teste
  # @a = 4
  # Não conseguiu enxergar essa variável

  @@a = 4

  def printa 
    puts @@a
  end
end

@a = 1
puts @a
puts Teste.new.printa

