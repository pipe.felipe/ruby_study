require "byebug"

a = 2

if a == 1
  puts a
else
  puts "Boa fe"
end

if a == 2 then puts "Uma linha só para a" end
if a == 2; puts "usando ;"; end

puts "oi" if a == 0

puts "==============================================\n\n"

if a == 2
  puts "a = 2"
elsif a == 3
  puts "a = 3"
else
  puts "a?"
end

case a
when 1
  puts "felipe 1"
when 2
  puts "felipe 2"
end

unless a == 1
  puts "o a é diferente de 1"
end

puts "o valor de a é: #{a}" if a == 2

a = a == 2 ? 20 : 21
puts "O valor final de a com o ternário é de: #{a}"
