# Pode ser usado símbolo ou stirng
# para fazer um Hash no ruby.
#
# NÃO DEIXE OS PENSAMENTOS NEGATIVOS DERRUBAR.

old_meu_hash = {
  "nome" => "felipe",
  :sobrenome => "alves",
  :idade => 25,
  :tristesa => 0.8,
  :felicidade => 0.7,
}

meu_hash = {
  nome: "felipe",
  sobrenome: "alves",
  idade: 25,
  tristesa: 0.8,
  felicidade: 0.7,
}

puts old_meu_hash["nome"]
puts meu_hash[:nome]

puts "Digite seu nome: "
meu_hash[:nome] = gets.delete("\n")

puts "O novo [:nome] é: " + meu_hash[:nome]
