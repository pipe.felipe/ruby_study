class Abstrata
  def initialize
    raise "classe não pode ser instanciada, somente herdada e implementada. Abstrata."
  end

  def test1
    raise "não implementada"
  end

  def test2
    "não implementada2"
  end
end
