class Interface
  def initialize
    raise "classe não pode ser instanciada, somente herdada e implementada. Interface."
  end

  def test1
    raise "Metodo para ser implementado"
  end

  def test2
    raise "Método2 para ser implementado"
  end
end
