# INTERFACE
#
# Classe que não tem implementação.
# Funciona como uma ideia.
# Desenho de tudo que precisa ser implementado a uma classe.
# Os dados não são implementados.
# Não tem instância.

# Abstração
# Mais completa que a interface, mas não há como fazer instâncias.
# Alguns métodos podem já estar implementados e outros são deixados
# vazio.

# Singleton
# Classe que tem instância única.
# Não há como criar uma nova instância.
# Classe singleton não tem como criar mais de uma.

require "byebug"
# require_relative "interface"
# require_relative "abstrata"
# require_relative "klass"

require_relative "singleton/singleton"

debugger

x = ""
