require "byebug"

# class Carro
#   def initialize(nome = "Modelo Padrão.")
#     @nome = nome
#   end

#   def nome=(value)
#     @nome = value
#   end

#   def nome
#     @nome
#   end

#   def pneu=(value)
#     @pneu = value
#   end

#   def pneu
#     @pneu
#   end

#   def porta=(value)
#     @porta = value
#   end

#   def porta
#     @porta
#   end

#   def painel=(value)
#     @painel = value
#   end

#   def painel
#     @painel
#   end

#   def roda=(value)
#     @roda = value
#   end

#   def roda
#     @roda
#   end

#   def mostrar(marca = "Marca Padrão")
#     puts "Marca: #{marca} - Modelo: #{self.nome}"
#   end
# end

# debugger
# carro = Carro.new
# Carro.new.mostrar
#

class Carro
  def initialize(nome = "Default Model")
    @nome = nome
  end

  attr_accessor :nome, :pneu, :porta, :painel, :roda

  def mostrar(marca = "Dafault brand")
    puts " #{marca} #{self.nome}"
  end
end

carro = Carro.new("Fiesta")
carro.mostrar("Pink Paw")
