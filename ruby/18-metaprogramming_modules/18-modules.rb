require "byebug"

# felipe = "teste"

# def felipe.olha
#   "teste felipe"
# end

# a = felipe.clone
# a.olha

# b = felipe.dup
# # b.olha

# cc = a.dup

# debugger

# def cc.novo
#   2
# end

# clone = "copia variáveis e metodos"
# dup = "publica sem a cópia de metaprogramação"

# felipe = "teste"

# def felipe.olha1
#   "teste felipe"
# end

# def felipe.olha2
#   "teste felipe"
# end

# def felipe.olha3
#   "teste felipe"
# end

# debugger

# class << felipe
#   def olha1
#     "teste felipe"
#   end

#   def olha2
#     "teste felipe"
#   end

#   def olha3
#     "teste felipe"
#   end
# end

# felipe.rodar

# class Felipe
#   def self.ola
#     "Olá Felipe."
#   end

# Esse mesmo jeito mostrado acima.
# É semelhante ao demais, mas utilizando menos digitação
# não precisa ficar digitando self toda hora para cirar
# os métodos static.

# class << self
#   def hi
#     "felipe say hi."
#   end
# end
# end

# debugger

# Felipe.hi

# class Felipe
# def self.nomeando=(value)
#   @nomeando = value
# end

# def self.nomeando
#   @nomeando
# end

# class << self
#   attr_accessor :nome, :tel
# end
# end

# debugger

# module Utilidades
#   class Cpf
#     def validar
#       "validando cpf"
#     end
#   end

#   class Cnpj
#     def validar
#       "validando cnpj"
#     end
#   end
# end

# Utilidades::Cnpj

# module Utilidades
#   def validar_cpf
#     "validar_cpf"
#   end

#   def validar_cnpj
#     "validar_cnpj"
#   end
# end

# class Cliente
#   include Utilidades
# end

# module Utilidades
#   def validar_cpf
#     "validar_cpf"
#   end

#   def validar_cnpj
#     "validar_cnpj"
#   end
# end

# a = "ss"
# a.extend Utilidades

# module Teste
#   def oi
#     "olá"
#   end

#   def tirar_espacos
#     self.gsub(" ", "-")
#   end
# end

# module Teste2
#   def oi
#     "olá"
#   end

#   def de_classe
#     "de classe"
#   end
# end

# Include funciona nas instância.
# String.include Teste
# Extends funciona nas classes.
# String.extend Teste2

# debugger

# Utilizando o modulo Teste
# Os dois códigos abaixo tem o mesmo comportamento.
# Include inclui na instância ( mas quando é usado
# com o self( que no caso é a classe ) ele herda o
# comportamento diretamente na classe ).
#
# Agora falando o extend, esse já injeta diretamente
# na classe fazendo exatamente o mesmo comportamento
# do código abaixo.

# class Testando
#   class << self
#     include Teste
#   end
# end

# class Testando
#   extend Teste
# end

# module InstanciaEClasse
#   def instancia
#     "metodo de instância"
#   end

#   module Classe
#     def de_classe
#       "método de classe"
#     end
#   end
# end

# class Teste
#   include InstanciaEClasse
#   extend InstanciaEClasse::Classe
# end

module InstanciaEClasse
  def instancia
    "metodo de instância"
  end

  def instancia2
    "metodo de instância"
  end

  def self.included(cls)
    puts "-----#{cls}-----"
    cls.extend Classe
  end

  module Classe
    def de_classe
      "método de classe"
    end
  end
end

# class Teste
#   include InstanciaEClasse
#   extend InstanciaEClasse::Classe
# end

class Teste
  include InstanciaEClasse
end

Teste.new.instancia
Teste.de_classe

debugger
