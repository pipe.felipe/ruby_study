require "byebug"

#Hooks para módulos.
module UmModulo

  # Hook
  def self.included(klass)
    puts "Modulo #{self} incluido em #{klass}"
  end

  # Outro Hook
  def self.extended(klass)
    puts "Modulo #{self} extendido em #{klass}"
  end

  def instancia
    "metodo de instância"
  end

  def classe
    "metodo de classe"
  end
end

class Teste
  # essa é uma ação. Qua ativa a reação em self.included
  include UmModulo
end

class Teste2
  extend UmModulo
end

# Hooks para classes.

# class Pai
#   def self.inherited(klass)
#     puts "A Classe #{klass} herda de #{self}."
#   end
# end

class Pai
  def self.inherited(klass)
    puts "A Classe #{klass} herda de #{self}."

    # Se a variável for nula, o valor é colocado dentro desse array
    # caso contrário mantém o valor que já está armazenado.
    @classes ||= []

    # Agora que temos o array já declarado. Para adicionar valores à esse
    # array, utiliza-se o '<<' ou o class.push (que seria semelhante ao python)
    @classes << klass
  end

  def self.lista_de_classes
    @classes
  end
end

class Filho < Pai
  def mostra_nome
    "Android 18"
  end
end

class Filho2 < Pai
  def mostra_nome
    "Android 18 mais linda"
  end
end

class Filho3 < Pai
  def mostra_nome
    "Android 18 mais fofa"
  end
end

class Filho4 < Pai
  def mostra_nome
    "Android 18 version 2"
  end
end

puts Pai.lista_de_classes

Pai.lista_de_classes.map { |f| f.new.mostra_nome }

debugger
x = ""
