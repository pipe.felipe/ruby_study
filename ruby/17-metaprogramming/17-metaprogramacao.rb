require "byebug"

# def String.mostrar_nova_string2
#   "nova string"
# end

# a = "felipe"

# debugger

# def a.mostrar_nova_string
#   "nova string"
# end

# x = " "
puts "[[[Início #{self} ]]]"

class Carro
  puts "[[[Class: #{self} ]]]"

  def initialize(nome = "Modelo")
    @nome = nome
  end

  attr_accessor :nome, :porta, :painel, :roda
  attr_reader :pneu

  def mostrar(marca = "Padrão")
    puts "[[[Mostrar: #{self} ]]]"
    puts "Marca: #{marca} - Modelo: #{self.nome}"
  end

  # Semelhante ao static
  def self.metodo_de_classe
    "de classe"
  end
end

def Carro.metodo_de_classe2
  "de classe"
end

puts "[[[Fim: #{self} ]]]"

fiesta = Carro.new
golf = Carro.new

# def fiesta.mostrar
#   "Mostrar do fiesta"
# end

# def golf.mostrar
#   "Mostrar do golf"
# end

debugger

x = ""
