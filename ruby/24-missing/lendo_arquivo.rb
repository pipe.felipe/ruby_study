require 'byebug'

class Module
  def const_missing(nome)
    file = "database/#{nome}.csv"
    exists = File.exist?(file)

    unless exists
      puts "Class #{nome} não encontrado. Não existe nenhum csv na database."
      return
    end

    const_set(nome, Class.new)
    klass = const_get(nome)

    File.open(file) do |arquivo|
      arquivo.each do |linha|
        campos = linha.split(';')

        campos.each do |campo|
          klass.class_eval do
            define_method("#{campo}=") do |value|
              instance_variable_set "@#{campo}", value
            end

            define_method("#{campo}") do
              instance_variable_get "@#{campo}"
            end
          end
        end

        break
      end
    end

    klass
  end
end

f = Felipe.new
f.name="Felipe Batocciero."
puts f.name
