# frozen_string_literal: true
require 'byebug'

# study
class Felipe
  TESTE = 123

  def self.const_missing(nome)
    puts "There is no #{nome} in this class."
  end
end

puts Felipe::TESTE
