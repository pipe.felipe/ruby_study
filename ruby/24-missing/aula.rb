require "byebug"

class Felipe
  # @param [Object] nome
  def self.method_missing(nome, parametro)

    nome = nome.to_s
    if nome =~ /busca_por_/
      puts "Buscando no arquivo por #{nome.gsub("busca_por_", "")}, com o valor do #{parametro}"
      return
    end
    puts "Método de classe #{nome}, não existe na classe #{self}."
  end

  def method_missing(nome)
    nome = nome.to_s
    File.open("database/Felipe.csv") do |arquivo|
      arquivo.each do |linha|
        campos = linha.split(";")

        unless campos.include?(nome)
          puts "Campo #{nome} não exite no csv database."
        end
      end

      return nil
    end
  end
end

puts Felipe.busca_por_qualqurcoisa("outra_coisa_qualquer")