require 'byebug'

class Felipe
  def ola
    'Olá'
  end
end

# class Felipe2 < Felipe
#   def ola
#     "Olá sobrescrito - " + super
#   end
# end

class Felipe3 < Felipe
  alias_method :original_ola, :ola

  def ola
    'Olá sobrescrito.'
  end

  def todos
    "#{send('original_ola')} #{ola}"
    #{original_ola} ---------------- #{ola}"
  end
end

felipe3 = Felipe3.new
puts felipe3.ola
puts felipe3.original_ola
puts felipe3.todos

# module ModuloOverWrite
#   def metodo(nome)
#     original_metodo_nome = "#{nome.to_s}_original"
#     alias_method original_metodo_nome, nome

#     define_method(nome) do
#       'Teste para mostrar outras coisas dentro do módulo.'
#     end

#     define_method('todos') do
#       send(original_metodo_nome) + ' : ' + send(nome)
#     end
#   end
# end

module ModuloOverWrite
  def metodo(nome, &bloco)
    original_metodo_nome = "#{nome.to_s}_original"
    alias_method original_metodo_nome, nome

    define_method(nome, &bloco)

    define_method('todos') do
      send(original_metodo_nome) + ' : ' + send(nome)
    end
  end
end

class Felipe4 < Felipe
  extend ModuloOverWrite
  metodo :ola do
    'Gostaria de estar começando a alsar voo.'
  end
end

felipe4 = Felipe4.new
debugger
puts felipe4.ola
