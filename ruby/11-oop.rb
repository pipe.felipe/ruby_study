require "byebug"

# criando uma class vazia
# class Vazio; end

class Carro

  # Construtor
  def initialize(nome)
    @nome = nome
  end

  # desse modo; o ruby já entende que é um setter.
  def nome=(value)
    @nome = value
  end

  def nome
    @nome
  end

  def marca(marca)
    puts "Marca: #{marca} - Modelo: #{self.nome}"
  end
end

#debugger

carro = Carro.new("Fiesta")

carro.marca("Ford")
