require "byebug"

class Teste; end

Teste.class_eval do
  def metodo1
    puts "metodo de instancia 1 | class eval"
  end

  def metodo2
    puts "metodo de instancia 2 | class eval"
  end
end

Teste.new.metodo1
Teste.new.metodo2

# Class eval é útil para atribuir métodos
# para objetos já definidos.

# esse método é para a instância
String.class_eval do
  def teste
    "felipe"
  end
end

String.instance_eval do
  def teste_classe
    "esse é para a classe String."
  end
end

class << String
  def metodo
    "mesma coisa acima."
  end
end

module AtributosDinamicos
  def atributo(atributo)
    class_eval %{
      def #{atributo}
        @#{atributo}
      end
      def #{atributo}=(value)
        @#{atributo} = value
      end
    }
  end
end

class Teste
  extend AtributosDinamicos
  atributo :nome
end

class ClasseBloco
  def metodo1
    "metodo1 classe bloco"
  end

  def metodo2
    "metodo 2 classe bloco"
  end

  def fique_avontade(&bloco)
    instance_eval(&bloco)
  end

  private

  def metodo3
    "metodo 3"
  end
end

classe_bloco = ClasseBloco.new

classe_bloco.fique_avontade do
  metodo1
  metodo2
end

debugger

x = "felipe"
