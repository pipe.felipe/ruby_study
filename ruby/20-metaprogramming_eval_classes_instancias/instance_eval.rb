require "byebug"

a = "puts 'felipe'"

eval(a)

class Teste
  def initialize
    @variavel = "felipe"
  end

  def variavel_method=(value)
    @variavel = value
  end

  private

  def variavel_method
    @variavel
  end
end

teste = Teste.new

puts teste.instance_eval { @variavel }

teste.instance_eval do
  puts self
end

teste.instance_eval do
  def metodo
    puts "metodo da instance_eval"
  end
end

teste.metodo

Teste.instance_eval do
  def metodo_de_classe1_com_eval
    puts "metodo_de_classe1"
  end

  def metodo_de_classe2_com_eval
    puts "metodo de classe 2"
  end
end

# Esse jeito mostrado abaixo
# é o mesmo que o mostrado acima
# com o instance_eval.

class Teste
  class << self
    def metodo_de_classe1_com_self
      puts "metodo_de_classe1"
    end

    def metodo_de_classe2_com_self
      puts "metodo de classe 2"
    end
  end
end

debugger

x = "felipe alves"
